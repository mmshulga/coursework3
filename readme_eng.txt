Название: Cryptosystem on Elliptic Curve Points Group over Finite Polynomial Field

3 курс

Выполнил: Шульга Михаил Михайлович, mmshulga@gmail.com

Аннотация:
Features:
1. can generate public and private keys;
2. open public and private keys;
3. generate digital signature for a file;
4. check if given signature is valid for a file;
5. encrypt message (latin alphabet only);
6. decrypt message.